![Logo Serie Desarrollo de Videojuegos](banner.png)

# Serie Desarrollo de Videojuegos - Módulo II

## ~ Phaser a Fondo ~

### Contenidos del módulo I:
   - Pensar desde un inicio en la división de tareas.
   - Estructura y técnicas para escalar nuestro juego.
   - Lenguaje para la construcción de niveles.
   - Más sobre escenas y physics, tilemaps y debug mode.
   - Construcción de un nivel en nuestro videojuego.
   - Construcción de algunos game objects.

### Conceptos a repasar:
   - Un nuevo engine?: pensar desde un inicio en la división de tareas.
   - Nueva estructura y técnicas que nos permitirán escalar nuestro juego.
   - Un “nuevo lenguaje” para la construcción de niveles y sub-niveles.
   - Más sobre escenas y physics.
   - Implementación de tilemaps o tilesets.
   - Debug mode.   

### En la práctica:
   - Construcción de un sub-nivel.
   - Construcción de algunos game objects.
   - Trabajo práctico.   

## Instalación:
#### Clonar el repositorio (y especificar un nombre de carpeta):
```
git clone https://gitlab.com/gaspoker/gamedev-modulo-ii.git FOLDER_NAME
```

#### Navegar dentro de la nueva carpeta:
```
cd FOLDER_NAME
```

## Slides

### Construcción de un subnivel:
![Trabajo Práctico](sub-level.png)

### Construcción de elevadores:
![Trabajo Práctico](lifts.png)

### Construcción de palos de fuego:
![Trabajo Práctico](firewall-sticks.png)

## Licencia de uso

This gamedev serie is under MIT License. Copyright (c) 2020-2024 by Gaspo Soft (gaspoker@gmail.com).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

![Gaspo Soft Logo](gaspo-soft.png)