import { Loader } from './scenes/Loader.js';
import { Main } from './scenes/Main.js';
import { World } from './scenes/World.js';
import { Intro } from './scenes/Intro.js';
import { Game } from './scenes/Game.js';

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LEARNING POINT (1)
// >>> EMPEZAR POR AQUI !!!  (https://newdocs.phaser.io/docs/3.60.0/Phaser.Types.Core.GameConfig)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var config = {
    title: 'Super Mario Bross (C) 1985 Nintendo',
    fps: 60, // Game loop configuration.
    type: Phaser.AUTO, // Which renderer to use. Phaser.AUTO, Phaser.CANVAS, Phaser.HEADLESS, or Phaser.WEBGL. AUTO picks WEBGL if available, otherwise CANVAS.
    width: Game.SCENE_WIDTH,
    height: Game.SCENE_HEIGHT,
    antialias: false, // Sets the antialias property when the WebGL context is created. Setting this value does not impact any subsequent textures that are created, or the canvas style attributes.
    pixelArt: true, // Prevent pixel art from becoming blurred when scaled. It will remain crisp (tells the WebGL renderer to automatically create textures using a linear filter mode).
    roundPixels: true, // Draw texture-based Game Objects at only whole-integer positions. Game Objects without textures, like Graphics, ignore this property.
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH, 
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: true
        }
    },
    scene: [
        Loader,
        Main,
        World,
        Intro,
        Game
    ]
};

var game = new Phaser.Game(config);