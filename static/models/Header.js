/**
 * Class Header
 */
export class Header extends Phaser.GameObjects.Container {

    constructor(scene, score, coins, stage, time) {
        super(scene);
        this.score = score || 0;
        this.coins = coins || 0;
        this.stage = stage;
        this.time = time;

        let marioText = this.scene.add.bitmapText(24, 8, 'font-white', 'MARIO', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.scoreNumber = this.scene.add.bitmapText(24, 16, 'font-white', '000000', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        let worldText = this.scene.add.bitmapText(144, 8, 'font-white', 'WORLD', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.stageNumber = this.scene.add.bitmapText(152, 16, 'font-white', '0-0', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        let timeText = this.scene.add.bitmapText(200, 8, 'font-white', 'TIME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.timeNumber = this.scene.add.bitmapText(208, 16, 'font-white', '000', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        this.coinsNumber = this.scene.add.bitmapText(97, 16, 'font-white', 'x00', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1).setScrollFactor(0);
        let coinImage = this.scene.add.image(89, 16, 'score-coin').setOrigin(0).setScrollFactor(0);   

        this.add([ marioText, this.scoreNumber, worldText, this.stageNumber, timeText, this.timeNumber, this.coinsNumber, coinImage ]);
        this.scene.add.existing(this); // Add the container to the scene

        this.updateScore();
        this.updateCoins();
        this.updateStage();
        this.updateTime();
    }

    getScore() {
        return this.score;
    }

    setScore(score) {
        this.score += score;
        this.updateScore(this.score);
    }

    incCoins() {
        this.coins++;
        this.updateCoins();
    }
    
    getCoins() {
        return this.coins;
    }

    decTime() {
        this.time--;
        this.updateTime();
    }

    getTime() {
        return this.time;
    }

    startTimer(timeOutCallback) {
        if (this.time) {
            this.timer = this.scene.time.addEvent({
                delay: 1000, // 1 second
                loop: true,
                callback: () => {
                    this.decTime();
                    if (this.time == 0) {                    
                        timeOutCallback();
                        this.stopTimer();
                    }
                }
            });
        }
    }

    stopTimer() {
        if (this.timer) {
            this.timer.remove();
            this.timer = null;
        }
    }

    updateScore() {
        let text = (`000000${this.score}`).slice(-6);
        this.scoreNumber.setText(text);
    }

    updateCoins() {
        let text = 'x' + (`00${this.coins}`).slice(-2);
        this.coinsNumber.setText(text);
    }

    updateStage() {
        this.stageNumber.setText(this.stage);
    }

    updateTime() {
        let text = (`00${this.time}`).slice(-3);
        this.timeNumber.setText(text);
    }


    toggleTimer(pause) {
        if (this.timer) {
            this.timer.paused = pause;
        }
    }

}