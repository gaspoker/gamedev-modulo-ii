/**
 * Mario class
 */
export class Mario extends Phaser.Physics.Arcade.Sprite {
    static get VELOCITY() { return 100; }
    static get JUMP_VELOCITY() { return 200; }

    constructor(scene, x, y, size) {
        super(scene);
        this.scene = scene;

        this.lives = 3; // TODO
        this.alive = true;
        this.slide = false;
        this.protected = false;
        this.size = size || 'small';
        this.setTexture(`mario-${this.size}`);
        this.setDepth(2);
        this.setOrigin(0);
        this.setPosition(x, y);

        this.scene.add.existing(this); // Add the sprite to the scene
        this.scene.physics.add.existing(this); // Add the sprite body to the scene
    }

    isAlive() {
        return this.alive;
    }

    fire() { // TODO
    }

    stand() {
        this.setVelocityX(0); // Sets the horizontal component of the body's velocity to zero
        this.anims.stop(); // Stop animation
        if (this.isOnGround()) {
            this.standing();
        } else {
            this.jumping();
        }
    }

    standing() {
        this.setFrame(0);
    }

    run() {
        let velocity = this.flipX ? Mario.VELOCITY * -1 : Mario.VELOCITY;
        this.setVelocityX(velocity);
    }

    running() {
        this.anims.play(`mario-${this.size}-runs-anim`, true); // Run animation
    }

    jump() {
        this.setVelocityY(Mario.JUMP_VELOCITY * -1);
        this.scene.jumpSmall.play(); // Play jump sound
    }

    jumping() {
        this.setFrame(5);
    }

    turnLeft() {
        this.flipX = true;
        if (this.isOnGround()) {
            this.running();
        } else {
            this.jumping();
        }
    }

    turnRight() {
        this.flipX = false;
        if (this.isOnGround()) {
            this.running();
        } else {
            this.jumping();
        }
    }

    stop(x) {
        this.setX(x);
    }

    isOnGround() {
        return this.body.touching.down;
    }

    isProtected() {
        return this.protected;
    }

    hurt() {
        if (!this.isProtected()) {
            if (this.isBig()) {
                this.shrink();
            } else {
                this.die()
            }
        }
    }

    die() {
        if (this.isAlive()) {
            this.scene.music.stop();
            this.alive = false;
            this.scene.tweens.add({
                targets: this,
                y: '-=48',
                ease: 'Cubic',
                duration: 400,
                yoyo: true,
                onStart: () => {                        
                    this.scene.dieSound.play();
                    this.scene.stopEnemies();
                },
                onComplete: () => {
                    this.setImmovable(true);
                    this.scene.restartGame();
                }
            });
        }
    }

    isBig() {
        return this.size == 'big';
    }
    
    dying() {
        this.setVelocityX(0);
        this.anims.stop();
        this.frizzing();
    }

    frizzing() {
        this.setFrame(13);
    }

    sliding() {
        this.anims.play(`mario-${this.size}-slide-down`, true); // Run animation
    }

    slideDown(x) {
        this.slide = true;
        this.setX(x);
        this.sliding();
    }

    isSliding() {
        return this.slide;
    }

    goCastleDoor() {
        this.anims.stop();
        this.x += 16;
        this.flipX = true;

        // Jump to groung
        this.scene.tweens.add({
            targets: this,
            x: '+=16',
            y: { from: this.y, to: 208 - this.height },
            ease: 'Linear',
            duration: 400,
            yoyo: false,
            onStart: () => {                        
                this.jumping();
            },
            onComplete: () => {
                this.flipX = false;
                // Run to the door
                this.scene.tweens.add({
                    targets: this,
                    x: '+=72',
                    ease: 'Linear',
                    duration: 600,
                    yoyo: false,
                    onStart: () => {                        
                        this.running();
                        // Move the camera
                        this.scene.tweens.add({
                            targets: this.scene.cameras.main,
                            scrollX: '+=64',
                            ease: 'Linear',
                            duration: 600,
                            yoyo: false                        
                        });
                    },
                    onComplete: () => {
                        // Enter in the castle
                        this.scene.tweens.add({
                            targets: this,
                            alpha: { from: 1, to: 0 },
                            ease: 'Linear',
                            duration: 400,
                            yoyo: false,
                            onComplete: () => {
                                this.scene.endWorld();                                
                            }
                        });
        
                    }
                });
            }
        });
    }

    grow() {
        let x = this.x, y = this.y;
        this.size = 'big';
        this.setVisible(false);
        this.scene.powerUp.play();
        let grow = this.scene.add.sprite(x, y - 16, 'mario-sizes').setDepth(1).setOrigin(0);
        grow.anims.play('mario-grows-anim', true);
        grow.on(Phaser.Animations.Events.ANIMATION_COMPLETE, () => {
            this.setTexture(`mario-${this.size}`);
            this.setPosition(x, y - 16);
            this.body.setSize(this.width, this.height, false);
            this.refreshBody();
            this.setVisible(true);
            grow.destroy();
        }, this.scene);
    }

    shrink() {
        let x = this.x, y = this.y;
        // Protect Mario for 3 seconds
        this.protected = true;
        this.scene.tweens.add({
            targets: this,
            alpha: { from: 1, to: 0 },
            ease: 'Linear',
            duration: 50,
            repeat: 60,
            yoyo: true,
            onComplete: () => {
                this.protected = false;
            }
        });
                
        this.size = 'small';
        this.setVisible(false);
        this.scene.powerUp.play();
        let shrink = this.scene.add.sprite(x, y, 'mario-sizes').setDepth(1).setOrigin(0);
        shrink.anims.play('mario-shrinks-anim', true);
        shrink.on(Phaser.Animations.Events.ANIMATION_COMPLETE, () => {
            this.setTexture(`mario-${this.size}`);
            this.setPosition(x, y + 16);
            this.body.setSize(this.width, this.height, false);
            this.refreshBody();
            this.setVisible(true);
            shrink.destroy();
        }, this.scene);

    } // shrink

    getSize() {
        return this.size;
    }

    hurtEnemy(enemy) {
        enemy.die = true;

        this.scene.tweens.add({
            targets: this,
            y: '-=8',
            ease: 'Linear',
            duration: 200,
            yoyo: false,
            onStart: () => {
                this.scene.stompSound.play();
                this.scene.addFlyingPoints(enemy.x, enemy.y - 16, '100');
                enemy.setVelocity(0);
                enemy.anims.stop();
                enemy.setFrame(2);
            },
            onComplete: () => {
                enemy.destroy();
            }
        });
    } // hurtEnemy

}