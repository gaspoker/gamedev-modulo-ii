/**
 * Class Menu
 */
export class Menu extends Phaser.GameObjects.Container {

    constructor(scene) {
        super(scene);

        let bannerImage = this.scene.add.image(40, 32, 'banner').setDepth(1).setOrigin(0);
        let copyrightText = this.scene.add.bitmapText(104, 120, 'font-pink', '@1985 NINTENDO', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        let player1Text = this.scene.add.bitmapText(88, 144, 'font-white', '1 PLAYER GAME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        let player2Text = this.scene.add.bitmapText(88, 160, 'font-white', '2 PLAYER GAME', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
        let mushroomImage = this.scene.add.image(72, 144, 'player-mushroom').setOrigin(0);
        this.topScoreNumber = this.scene.add.bitmapText(96, 184, 'font-white', 'TOP- 000000', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);

        this.add([ bannerImage, copyrightText, player1Text, player2Text, mushroomImage, this.topScoreNumber ]);
        this.scene.add.existing(this); // Add the container to the scene
    }

    setTopScore(topScore) {
        let text = (`000000${topScore}`).slice(-6);
        this.topScoreNumber.setText(`TOP- ${text}`);
    }

}