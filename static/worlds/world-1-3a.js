// WORLD 1-3a
export default {
    stageId: '1-3a',
    nextStageId: '1-4a',
    type: 'ow', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater

    // Mario position
    marioX: 32, marioY: 192,

    // Time
    time: 400,

    // Scene
    music: 'overworld',
    width: 2624, height: 240, // Pixels
    
    // Background
    color: '#5C94FC',
    clouds: [ {c:3, r:3, s:'medium'}, {c:9, r:7, s:'small'}, {c:18, r:2, s:'medium'}, {c:35, r:7, s:'small'}, {c:38, r:6, s:'small'}, {c:46, r:11, s:'small'}, {c:51, r:3, s:'medium'}, {c:57, r:7, s:'small'}, {c:66, r:2, s:'medium'}, {c:76, r:11, s:'small'},
              {c:83, r:7, s:'small'}, {c:86, r:6, s:'small'}, {c:94, r:11, s:'small'}, {c:99, r:3, s:'medium'}, {c:114, r:2, s:'medium'}, {c:124, r:11, s:'small'}, {c:131, r:7, s:'small'}, {c:134, r:6, s:'small'}, {c:142, r:11, s:'small'}, {c:147, r:3, s:'medium'}
            ],
    
    // Blocks
    groundBlocks: [ {c:{from:0, to:15}, r:{from:13, to:14}}, {c:{from:129, to:164}, r:{from:13, to:14}} ],
    questionBlocks: [ {c:59, r:10, t:'coin', p:'200'} ],
    stairBlocks: [ {c:{from:138, to:139}, r:{from:9, to:12}}, {c:{from:140, to:141}, r:{from:7, to:12}}, {c:{from:142, to:143}, r:{from:5, to:12}}, {c:152, r:12} ],
    cliffs: [ {c:{from:18, to:21}, r:{from:12, to:14}}, {c:{from:24, to:31}, r:{from:9, to:14}}, {c:{from:26, to:30}, r:{from:5, to:8}}, {c:{from:32, to:34}, r:{from:12, to:14}},
              {c:{from:35, to:39}, r:{from:8, to:14}}, {c:{from:40, to:46}, r:{from:4, to:14}}, {c:{from:50, to:53}, r:{from:13, to:14}}, {c:{from:59, to:63}, r:{from:13, to:14}}, {c:{from:60, to:63}, r:{from:5, to:12}},
              {c:{from:65, to:69}, r:{from:13, to:14}}, {c:{from:70, to:72}, r:{from:9, to:14}}, {c:{from:76, to:81}, r:{from:6, to:14}}, {c:{from:98, to:101}, r:{from:11, to:14}},
              {c:{from:104, to:111}, r:{from:7, to:14}}, {c:{from:113, to:115}, r:{from:13, to:14}}, {c:{from:116, to:119}, r:{from:9, to:14}}, {c:{from:122, to:125}, r:{from:9, to:14}}
            ],
    lifts: [ {c:55, r:6, d:'up', s:'big'},
             {c:86, cTo:91, r:8, d:'horiz', s:'small'},
             {c:92, cTo:100, r:9, d:'horiz', s:'small'},
             {c:127, cTo:134, r:10, d:'horiz', s:'small'}
            ], 
    coins: [ {c:{from:27, to:29}, r:4}, {c:33, r:11}, {c:{from:37, to:38}, r:2}, {c:{from:50, to:51}, r:6}, {c:{from:60, to:63}, r:4},
             {c:{from:85, to:86}, r:5}, {c:{from:93, to:94}, r:4}, {c:{from:97, to:98}, r:4}, {c:{from:113, to:115}, r:12}, {c:{from:120, to:121}, r:5}
           ],

    // Final
    flagPole: {c:152, r:2},
    finalFlag: {c:151, r:3},
    castles: [ {c:0, r:8, s:'small'}, {c:156, r:2, s:'big'} ],
    castleFlag: {c:160, r:3}, // TODO. Fireworks !
    
    // Enemies
}
