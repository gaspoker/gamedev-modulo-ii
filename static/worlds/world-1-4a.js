// WORLD 1-4a
export default {
    stageId: '1-4a',
    type: 'ca', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater

    // Mario position
    marioX: 40, marioY: 80,

    // Time
    time: 400,

    // Scene
    music: 'overworld',
    width: 2560, height: 240, // Pixels
    
    // Background
    color: '#000000',
    
    // Blocks
    bigBricks: [ {c:{from:0, to:159}, r:2}, {c:{from:0, to:23}, r:{from:3, to:4}}, {c:23, r:5}, {c:{from:37, to:71}, r:{from:3, to:5}}, {c:80, r:3}, {c:88, r:3}, {c:{from:97, to:103}, r:{from:3, to:4}}, {c:{from:123, to:127}, r:{from:3, to:4}}, {c:{from:142, to:143}, r:{from:3, to:5}},
                 {c:{from:0, to:4}, r:9}, {c:{from:0, to:3}, r:8}, {c:{from:0, to:2}, r:7}, {c:{from:0, to:12}, r:{from:10, to:14}}, {c:{from:15, to:25}, r:{from:10, to:14}}, {c:{from:29, to:31}, r:{from:10, to:14}}, {c:{from:35, to:71}, r:{from:9, to:14}}, {c:{from:72, to:103}, r:{from:10, to:14}}, {c:{from:104, to:127}, r:{from:13, to:14}}, {c:{from:116, to:119}, r:{from:10, to:12}}, {c:{from:123, to:127}, r:{from:10, to:12}}, {c:{from:141, to:143}, r:{from:9, to:12}}, {c:{from:141, to:159}, r:{from:13, to:14}},
            ],
    armoreds: [ {c:23, r:6}, {c:30, r:10}, {c:37, r:6}, {c:49, r:6}, {c:60, r:6}, {c:67, r:6}, {c:76, r:9}, {c:80, r:4}, {c:84, r:9}, {c:88, r:4}, {c:92, r:9} ],
    lava:[ {c:{from:13, to:14}, r:{from:12, to:14}}, {c:{from:26, to:28}, r:{from:13, to:14}}, {c:{from:32, to:34}, r:{from:13, to:14}}, {c:{from:128, to:140}, r:{from:13, to:14}} ],

    bridges:[ {c:{from:128, to:140}, r:10} ],
    axes:[ {c:141, r:8} ],
    chains:[ {c:140, r:9} ],
    questionBlocks: [ {c:30, r:6, t:'coin', p:'200'} ],
    lifts: [ {c:134, cTo:138, r:6, d:'horiz', s:'small'} ],
    fireSticks: [ {c:30, r:10, s:6}, {c:49, r:6, s:6}, {c:60, r:6, s:6}, {c:67, r:6, s:6}, {c:76, r:9, s:6}, {c:84, r:9, s:6}, {c:88, r:4, s:6} ],    

    littlePrincess: {c:152, r:11},

    // Enemies
    bowser: {c:138, r:7}
}
