// WORLD 1-2b
export default {
    stageId: '1-2b',
    type: 'ug', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater

    // Mario position
    marioX: 26, marioY: 32,

    // Scene
    music: 'underground',
    width: 256, height: 240, // Pixels

    // Background
    color: '#000000',

    // Blocks
    groundBlocks: [ {c:{from:0, to:15}, r:{from:13, to:14}} ],

    bricks: [ {c:0, r:{from:2, to:12}},
              {c:{from:3, to:14}, r:{from:2, to:5}},
              {c:{from:13, to:14}, r:{from:6, to:10}},
              {c:{from:3, to:12}, r:9}
            ],

    coins: [ {c:{from:4, to:11}, r:8}, {c:{from:3, to:11}, r:12} ],

    // Pipes
    pipeTubes: [ {c:13, r:11, t:'hor-ent', stageId:'1-2a', marioX: 1848, marioY: 144}, {c:14, r:11, t:'hor-ext'}, {c:15, r:11, t:'hor-end'},
                 {c:15, r:2, t:'ver-ext-left'}, {c:15, r:3, t:'ver-ext-left'}, {c:15, r:4, t:'ver-ext-left'}, {c:15, r:5, t:'ver-ext-left'}, {c:15, r:6, t:'ver-ext-left'}, {c:15, r:7, t:'ver-ext-left'}, {c:15, r:8, t:'ver-ext-left'}, {c:15, r:9, t:'ver-ext-left'}, {c:15, r:10, t:'ver-ext-left'}, {c:15, r:11, t:'ver-ext-left'}, {c:15, r:12, t:'ver-ext-left'}
               ],
    
}
