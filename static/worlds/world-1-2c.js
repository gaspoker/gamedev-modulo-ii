// WORLD 1-2c
export default {
    stageId: '1-2c',
    nextStageId: '1-3a',
    type: 'ow', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater

    // Mario position
    marioX: 56, marioY: 144,

    // Scene
    music: 'overworld',
    width: 560, height: 240, // Pixels
    
    // Background
    color: '#5C94FC',
    clouds: [ {c:4, r:2, s:'medium'}, {c:24, r:3, s:'small'} ],
    mountains: [ {c:16, r:10, s:'big'}, {c:32, r:11, s:'small'}, ],
    bushes: [ {c:29, r:12, s:'small'} ],
    
    // Blocks
    groundBlocks: [ {c:{from:0, to:34}, r:{from:13, to:14}} ],
    stairBlocks: [ {c:{from:5, to:13}, r:12}, {c:{from:6, to:13}, r:11}, {c:{from:7, to:13}, r:10}, {c:{from:8, to:13}, r:9}, {c:{from:9, to:13}, r:8}, {c:{from:10, to:13}, r:7}, {c:{from:11, to:13}, r:6}, {c:{from:12, to:13}, r:5},
                   {c:22, r:12}
                 ],
    
    // Final
    flagPole: {c:22, r:2},
    finalFlag: {c:21, r:3},
    castles: [ {c:26, r:8, s:'small'} ],
    castleFlag: {c:28, r:9},    

    // Pipes
    pipeTubes: [ {c:3, r:11, t:'ver-ent'} ],
}
