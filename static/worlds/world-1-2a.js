// WORLD 1-2a
export default {
    stageId: '1-2a',
    type: 'ug', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater
    
    intro: {
      // Background
      type: 'ow', // ow: Overworld / ug: Underground / ca: Castle / uw: Underwater
      music: 'overworld',
      color: '#5C94FC',
      clouds: [ {c:3, r:3, s:'medium'}, {c:9, r:7, s:'small'} ],
      castle: { c:0, r:8, s:'small' },
      pipeTubes: [ {c:10, r:11, t:'hor-ent'}, {c:12, r:11, t:'hor-end'}, {c:13, r:12, t:'ver-ext-right'}, {c:13, r:11, t:'ver-ext-right'}, {c:12, r:9, t:'ver-ent'} ],
      groundBlocks: [ {c:0, r:13}, {c:0, r:14}, {c:1, r:13}, {c:1, r:14}, {c:2, r:13}, {c:2, r:14}, {c:3, r:13}, {c:3, r:14}, {c:4, r:13}, {c:4, r:14}, {c:5, r:13}, {c:5, r:14}, {c:6, r:13}, {c:6, r:14}, {c:7, r:13}, {c:7, r:14}, {c:8, r:13}, {c:8, r:14}, {c:9, r:13}, {c:9, r:14}, {c:10, r:13}, {c:10, r:14}, {c:11, r:13}, {c:11, r:14}, {c:12, r:13}, {c:12, r:14}, {c:13, r:13}, {c:13, r:14}, {c:14, r:13}, {c:14, r:14}, {c:15, r:13}, {c:15, r:14}, {c:16, r:13}, {c:16, r:14} ],
      // Transition
      marioX: 32, marioY: 192, marioToX: 144,
      duration: 2000,
      completeSound: 'pipe-sound'
    },

    // Mario position
    marioX: 40, marioY: 32,

    // Time
    time: 400,

    // Scene
    music: 'overworld',
    width: 3072, height: 240, // Pixels
    
    // Background
    color: '#000000',
    
    // Blocks
    groundBlocks: [ {c:{from:0, to:79}, r:{from:13, to:14}}, {c:{from:83, to:119}, r:{from:13, to:14}}, {c:{from:122, to:123}, r:{from:13, to:14}}, {c:{from:126, to:137}, r:{from:13, to:14}}, {c:{from:145, to:152}, r:{from:13, to:14}}, {c:{from:160, to:191}, r:{from:13, to:14}} ],
    bricks: [ {c:0, r:{from:2, to:12}}, {c:{from:6, to:137}, r:2}, {c:29, r:8}, {c:39, r:{from:7, to:9}}, {c:41, r:{from:7, to:9}}, {c:44, r:{from:7, to:9}}, {c:46, r:{from:7, to:9}}, {c:40, r:9}, {c:{from:42, to:43}, r:7}, {c:45, r:9},
              {c:{from:52, to:53}, r:{from:5, to:9}}, {c:{from:54, to:55}, r:{from:3, to:4}}, {c:{from:54, to:55}, r:{from:9, to:11}}, {c:{from:58, to:63}, r:{from:3, to:4}}, {c:{from:62, to:63}, r:{from:5, to:9}}, {c:{from:58, to:63}, r:9},
              {c:{from:66, to:69}, r:{from:3, to:4}}, {c:67, r:{from:5, to:8}}, {c:{from:67, to:69}, r:9}, {c:69, r:8}, {c:{from:72, to:73}, r:{from:5, to:9}}, {c:{from:76, to:79}, r:{from:3, to:4}}, {c:{from:76, to:79}, r:9},
              {c:{from:84, to:89}, r:{from:7, to:8}}, {c:{from:122, to:123}, r:{from:10, to:12}}, {c:{from:145, to:150}, r:8}, {c:{from:160, to:176}, r:{from:10, to:12}}, {c:{from:161, to:167}, r:2}, {c:{from:170, to:186}, r:2}, {c:{from:170, to:176}, r:{from:3, to:9}}, {c:{from:190, to:191}, r:{from:2, to:12}}
            ],
    questionBlocks: [ {c:10, r:9, t:'coin', p:'200'}, {c:11, r:9, t:'coin', p:'200'}, {c:12, r:9, t:'coin', p:'200'}, {c:13, r:9, t:'coin', p:'200'}, {c:14, r:9, t:'coin', p:'200'} ],
    stairBlocks: [ {c:17, r:12}, {c:19, r:{from:11, to:12}}, {c:21, r:{from:10, to:12}}, {c:23, r:{from:9, to:12}}, {c:25, r:{from:9, to:12}}, {c:27, r:{from:10, to:12}}, {c:31, r:{from:10, to:12}}, {c:33, r:{from:11, to:12}},
                   {c:{from:133, to:137}, r:12}, {c:{from:134, to:137}, r:11}, {c:{from:135, to:137}, r:10}, {c:{from:136, to:137}, r:9}
                  ],                  
    lifts: [ {c:140, r:7, d:'down', s:'big'}, {c:140, r:13, d:'down', s:'big'}, {c:155, r:9, d:'up', s:'big'}, {c:155, r:1, d:'up', s:'big'} ],
    coins: [ {c:40, r:8}, {c:{from:41, to:44}, r:5}, {c:45, r:8}, {c:{from:58, to:61}, r:8}, {c:68, r:8}, {c:{from:84, to:89}, r:5} ],

    // Pipes
    pipeTubes: [ {c:103, r:10, t:'ver-ent', stageId:'1-2b'}, {c:103, r:12, t:'ver-ext'},
                 {c:109, r:9, t:'ver-ent'}, {c:109, r:11, t:'ver-ext'}, {c:109, r:12, t:'ver-ext'},
                 {c:115, r:11, t:'ver-ent'},
                 {c:166, r:8, t:'hor-ent', stageId:'1-2c'}, {c:168, r:8, t:'hor-end'}, {c:169, r:9, t:'ver-ext-right'}, {c:169, r:8, t:'ver-ext-right'}, {c:168, r:7, t:'ver-ext-left'}, {c:169, r:7, t:'ver-ext-right'}, {c:168, r:6, t:'ver-ext-left'}, {c:169, r:6, t:'ver-ext-right'}, {c:168, r:5, t:'ver-ext-left'}, {c:169, r:5, t:'ver-ext-right'}, {c:168, r:4, t:'ver-ext-left'}, {c:169, r:4, t:'ver-ext-right'}, {c:168, r:3, t:'ver-ext-left'}, {c:169, r:3, t:'ver-ext-right'}, {c:168, r:2, t:'ver-ext-left'}, {c:169, r:2, t:'ver-ext-right'},
                 {c:178, r:10, t:'ver-ent'}, {c:178, r:12, t:'ver-ext'},
                 {c:182, r:10, t:'ver-ent'}, {c:182, r:12, t:'ver-ext'},
                 {c:186, r:10, t:'ver-ent'}, {c:186, r:12, t:'ver-ext'},
               ]
    
    // Enemies
}
