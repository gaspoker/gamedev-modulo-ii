import { Game } from './Game.js';
import { Header } from '../models/Header.js';

/**
 * Class World
 */
export class World extends Phaser.Scene {

    constructor() {
        super('World');
    }

    create(data) {      
        console.log('data.stageId: ' + data.stageId);        
        data.world = this.getWorld(data.stageId);
        
        data.marioX = data.marioX || data.world.marioX;
        data.marioY = data.marioY || data.world.marioY;
        data.time = data.time || data.world.time;
        data.stage = data.stageId.slice(0, -1);

        if (!data.skip) {
            // HEADER
            new Header(this, data.score, data.coins, data.stage, data.time);

            this.add.bitmapText(88, 64, 'font-white', 'WORLD ' + data.stage, 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
            this.add.bitmapText(124, 88, 'font-white', 'x 3', 8, Phaser.GameObjects.BitmapText.ALIGN_CENTER).setDepth(1);
            this.add.sprite(100, 80, 'mario-small').setOrigin(0);

            this.time.addEvent({
                delay: 2000,
                callback: () => {
                    this.startGame(data);
                },
                loop: false
            });

        } else {
            this.startGame(data);
        }
    }

    getWorld(stageId) {
        for (let world of Game.WORLDS ) {
            if (world.stageId == stageId) {
                return world;
            }
        }
    }

    startGame(data) {
        // Skip stage screen and intro
        let nextScene = (!data.skip && data.world.intro) ? 'Intro' : 'Game';
        this.scene.start(nextScene, data);
    }

}