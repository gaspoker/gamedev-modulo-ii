import { Game } from './Game.js';
import { Header } from '../models/Header.js';
import { Menu } from '../models/Menu.js';

// Top score global variable
var topScore = 0;

/**
 * Main class
 */
export class Main extends Phaser.Scene {

    constructor () {
        super('Main'); // Scene key
    }

    init(lastGame) {
        // SCORING
        let lastScore = !lastGame.score ? 0 : lastGame.score;
        topScore = lastScore > topScore ? lastScore : topScore;
    }

    create() {
        this.cameras.main.setBackgroundColor('#5C94FC');
        this.add.image(0 * Game.BLOCK_SIZE, 10 * Game.BLOCK_SIZE, 'structure-tiles', 'mountain-big').setDepth(0).setOrigin(0);
        this.add.image(11 * Game.BLOCK_SIZE, 12 * Game.BLOCK_SIZE, 'structure-tiles', 'bush-big').setDepth(0).setOrigin(0);

        for (let c=0; c<16; c++) {
            for (let r=13; r<15; r++) {
                this.add.image(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', 'ow-ground').setDepth(1).setOrigin(0);
            }
        }
                
        // MARIO
        this.add.image(40, 192, 'mario-small').setDepth(0).setOrigin(0);

        // HEADER
        new Header(this);

        // MENU
        new Menu(this);

        // Press a key to start the game
        this.input.keyboard.on('keydown', (event) => { 
            this.scene.start('World', { stageId: Game.INIT_STAGE_ID, skip: false} );
        });

    }   

}

