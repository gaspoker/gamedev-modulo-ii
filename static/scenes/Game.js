import { Mario } from '../models/Mario.js';
import { Header } from '../models/Header.js';
import World_1_1a from '../worlds/world-1-1a.js';
import World_1_1b from '../worlds/world-1-1b.js';
import World_1_2a from '../worlds/world-1-2a.js';
import World_1_2b from '../worlds/world-1-2b.js';
import World_1_2c from '../worlds/world-1-2c.js';
import World_1_3a from '../worlds/world-1-3a.js';
import World_1_4a from '../worlds/world-1-4a.js';

export class Game extends Phaser.Scene {
    static get INIT_STAGE_ID() { return '1-1a'; }
    static get SCENE_WIDTH() { return 256; }
    static get SCENE_HEIGHT() { return 240; }
    static get CAMERA_MOTION_POINT() { return 96; }
    static get CAMERA_VELOCITY() { return 2; }
    static get BLOCK_SIZE() { return 16; }
    static get FIREBALL_SIZE() { return 8; }
    static get BOWSER_SIZE() { return 32; }
    static get ENEMY_VELOCITY() { return -40; }
    static get LIFT_VELOCITY() { return 20; }
    static get END_GAME_DELAY() { return 3000; }
    static get WORLDS() { return [ World_1_1a, World_1_1b, World_1_2a, World_1_2b, World_1_2c, World_1_3a, World_1_4a ]; }

    constructor () {
        super('Game'); // Scene key
    }

    init() {
        this.debug = false; // Enable/disable with ESC key
        this.audio = true; // Enable/disable with A key
        this.worldEnds = false;
    }

    create(data) {
        this.world = data.world;

        // HEADER
        this.header = new Header(this, data.score, data.coins, data.stage, data.time);
        this.header.startTimer(() => { this.mario.die(); });

        // MARIO
        this.mario = new Mario(this, data.marioX, data.marioY, data.marioSize);

        // CAMARA POSITION
        let posX = data.marioX - Game.CAMERA_MOTION_POINT < 0 ? 0 : data.marioX - Game.CAMERA_MOTION_POINT;
        this.cameras.main.scrollX = posX;
        this.cameras.main.setBackgroundColor(this.world.color);


        // STRUCTURES
        // Clouds
        if (this.world.clouds) {
            for (let block of this.world.clouds) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'cloud-' + block.s).setDepth(0).setOrigin(0);
            }
        }       

        // Mountains
        if (this.world.mountains) {
            for (let block of this.world.mountains) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'mountain-' + block.s).setDepth(0).setOrigin(0);
            }
        }       

        // Bushes
        if (this.world.bushes) {
            for (let block of this.world.bushes) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'bush-' + block.s).setDepth(0).setOrigin(0);
            }
        }    
        
        // Chains
        if (this.world.chains) {
            for (let block of this.world.chains) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'chain').setDepth(0).setOrigin(0);
            }
        } 

        // Axes
        if (this.world.axes) {
            for (let block of this.world.axes) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'axe').setDepth(0).setOrigin(0);
            }
        } 

        this.blocks = this.physics.add.staticGroup();

        // Ground Blocks
        if (this.world.groundBlocks) {
            for (let block of this.world.groundBlocks) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-ground').setDepth(1).setOrigin(0).refreshBody();
                    }
                }
            }
        }

        // Brick Blocks
        if (this.world.bricks) {
            for (let block of this.world.bricks) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-brick').setDepth(1).setOrigin(0).refreshBody();
                    }
                }
            }
        }

        // Shiny Brick Blocks
        if (this.world.shinyBricks) {
            for (let block of this.world.shinyBricks) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-shiny-brick').setDepth(1).setOrigin(0).refreshBody();
                    }
                }                
            }
        }

        // Big Brick Blocks
        if (this.world.bigBricks) {
            for (let block of this.world.bigBricks) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        if (c==cFrom || c==cTo || r==rFrom || r==rTo) {
                            this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-big-brick').setDepth(1).setOrigin(0).refreshBody();
                        } else {
                            this.add.image(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-big-brick').setDepth(1).setOrigin(0);
                        }
                    }
                }                
            }
        }

        // Armored Blocks
        if (this.world.armoreds) {
            for (let block of this.world.armoreds) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-armored').setDepth(1).setOrigin(0).refreshBody();
                    }
                }                
            }
        }        

        // Bridges
        if (this.world.bridges) {
            for (let block of this.world.bridges) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'structure-tiles', 'bridge').setDepth(1).setOrigin(0).refreshBody();
                    }
                }                
            }
        }        

        // Lava
        if (this.world.lava) {
            for (let block of this.world.lava) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        if (r==rFrom) {
                            this.add.image(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'structure-tiles', 'lava-surface').setDepth(3).setOrigin(0);
                        } else {
                            this.add.image(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'structure-tiles', 'lava-depth').setDepth(3).setOrigin(0);
                        }
                    }
                }
            }
        }                

        // Question Blocks
        this.questionBlocks = this.physics.add.staticGroup();   

        if (this.world.questionBlocks) {
            if (!this.textures.exists('question-block')) {
                this.textures.addSpriteSheetFromAtlas('question-block', { atlas: 'item-tiles', frame: this.world.type + '-question-sprite', frameWidth: 16, frameHeight: 16 });
            }

            this.anims.create({
                key: 'question-block-anim',
                frames: this.anims.generateFrameNumbers('question-block', { frames: [ 0, 1, 2, 1, 0 ] }),
                frameRate: 6,
                repeat: -1
            });

            if (!this.textures.exists('spincoin')) {
                this.textures.addSpriteSheetFromAtlas('spincoin', { atlas: 'item-tiles', frame: this.world.type + '-spincoin-sprite', frameWidth: 16, frameHeight: 16 });
            }

            this.anims.create({
                key: 'spincoin-anim',
                frames: this.anims.generateFrameNumbers('spincoin', { frames: [ 3, 2, 1, 0 ] }),
                frameRate: 20,
                repeat: 4
            });
        
            for (let block of this.world.questionBlocks) {
                let sprite = this.questionBlocks.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'question-block').setDepth(1).setOrigin(0);
                sprite.t = block.t; // Texture
                sprite.p = block.p; // Points
                sprite.body.x += 1;
                sprite.body.width = 14; // Truco para poder pegarle cuando el bloque está rodeado por otros bloques
                sprite.body.height = 17; // Truco para poder pegarle cuando el bloque está rodeado por otros bloques
                sprite.play('question-block-anim', true);
                sprite.refreshBody();
            }        
        }
        
        // Coins
        this.coins = this.physics.add.staticGroup();

        if (this.world.coins) {
            if (!this.textures.exists('coin')) {            
                this.textures.addSpriteSheetFromAtlas('coin', { atlas: 'item-tiles', frame: this.world.type + '-coin-sprite', frameWidth: 16, frameHeight: 16 });
            }
            this.anims.create({
                key: 'coin-anim',
                frames: this.anims.generateFrameNumbers('coin', { frames: [ 0, 1, 2, 1, 0 ] }),
                frameRate: 6,
                repeat: -1
            });

            for (let block of this.world.coins) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        let sprite = this.coins.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'item-tiles', this.world.type + '-coin-sprite').setDepth(1).setOrigin(0);
                        sprite.play('coin-anim', true);
                        sprite.refreshBody();
                    }
                }                
            }
        }

        // Fire Sticks
        this.fireballs = [];
        if (this.world.fireSticks) {
            if (!this.textures.exists('fireball')) {            
                this.textures.addSpriteSheetFromAtlas('fireball', { atlas: 'item-tiles', frame: 'fireball-sprite', frameWidth: Game.FIREBALL_SIZE, frameHeight: Game.FIREBALL_SIZE });
            }
            this.anims.create({
                key: 'fireball-anim',
                frames: this.anims.generateFrameNumbers('fireball', { frames: [ 0, 1, 2, 3 ] }),
                frameRate: 6,
                repeat: -1
            });

            for (let block of this.world.fireSticks) {
                let count = 0;          
                let angle = Phaser.Math.FloatBetween(0.02, 0.06);
                for (let r=block.r; r<=block.r+block.s-1; r++) {
                    let x = block.c * Game.BLOCK_SIZE + Game.FIREBALL_SIZE / 2;
                    let y = block.r * Game.BLOCK_SIZE + count * Game.FIREBALL_SIZE + Game.FIREBALL_SIZE / 2;
                    if (r==block.r) {
                        let sprite = this.add.sprite(x, y, 'item-tiles', 'fireball-sprite').setDepth(3).setOrigin(0);
                        sprite.play('fireball-anim', true);
                    } else {
                        let fireball = this.physics.add.staticGroup();
                        fireball.x = x;
                        fireball.y = block.r * Game.BLOCK_SIZE + Game.FIREBALL_SIZE / 2;
                        fireball.angle = angle;
                        fireball.distance = y - fireball.y;
    
                        let sprite = fireball.create(x, y, 'item-tiles', 'fireball-sprite').setDepth(3).setOrigin(0);
                        sprite.play('fireball-anim', true);
                        sprite.refreshBody();
                        this.fireballs.push(fireball);
                    }
                    count++;
                }
            }
        }

        // Pipe Tubes
        if (this.world.pipeTubes) {
            for (let block of this.world.pipeTubes) {
                let newPipe = this.blocks.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', this.world.type + '-' + block.t).setDepth(2).setOrigin(0).refreshBody();

                // Pipe entrance
                if (block.stageId) {
                    this.physics.add.collider(this.mario, newPipe, (mario, pipe) => {
                        if (this.input.keyboard.enabled) {
                            // Set new scene data info
                            let data = {
                                stageId: block.stageId,
                                skip: true,
                                marioX: block.marioX,
                                marioY: block.marioY,
                                marioSize: this.mario.getSize(),
                                score: this.header.getScore(),
                                coins: this.header.getCoins(),
                                time: this.header.getTime()
                            };

                            if (block.t.startsWith('hor') && mario.body.touching.right && this.cursors.right.isDown) {
                                this.input.keyboard.enabled = false;
                                this.mario.stand();
                                if (this.audio) this.pipeSound.play();
                                this.tweens.add({
                                    targets: mario,
                                    x: '+=16',
                                    ease: 'Linear',
                                    duration: 1500,
                                    onComplete: () => {
                                        this.scene.start('World', data );
                                    },
                                });
                            } else if (block.t.startsWith('ver') && mario.body.touching.down && this.cursors.down.isDown) { 
                                this.input.keyboard.enabled = false;
                                this.mario.stand();
                                if (this.audio) this.pipeSound.play();
                                this.tweens.add({
                                    targets: mario,
                                    y: '+=32',
                                    ease: 'Linear',
                                    duration: 1500,
                                    onComplete: () => {
                                        this.scene.start('World', data );
                                    },
                                });
                            }
                        }
                    });

                }
            }
        }

        // Stair Blocks
        if (this.world.stairBlocks) {
            for (let block of this.world.stairBlocks) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                for (let c=cFrom; c<=cTo; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-stair').setDepth(1).setOrigin(0).refreshBody();
                    }
                }                
            }
        }

        // Cliffs
        if (this.world.cliffs) {
            for (let block of this.world.cliffs) {
                let cFrom = block.c.from !== undefined ? block.c.from : block.c;
                let cTo = block.c.to !== undefined ? block.c.to : block.c;
                let rFrom = block.r.from !== undefined ? block.r.from : block.r;
                let rTo = block.r.to !== undefined ? block.r.to : block.r;
                this.blocks.create(cFrom * Game.BLOCK_SIZE, rFrom * Game.BLOCK_SIZE, 'structure-tiles', 'cliff-left').setDepth(1).setOrigin(0).refreshBody();
                this.blocks.create(cTo * Game.BLOCK_SIZE, rFrom * Game.BLOCK_SIZE, 'structure-tiles', 'cliff-right').setDepth(1).setOrigin(0).refreshBody();
                for (let c=cFrom+1; c<=cTo-1; c++) {
                    for (let r=rFrom; r<=rTo; r++) {
                        if (r==rFrom) {
                            this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'structure-tiles', 'cliff-center').setDepth(1).setOrigin(0).refreshBody();
                        } else {
                            this.blocks.create(c * Game.BLOCK_SIZE, r * Game.BLOCK_SIZE, 'structure-tiles', 'cliff-base').setDepth(1).setOrigin(0).refreshBody();
                        }
                    }
                }
            }
        }

        // Armored Blocks
        if (this.world.hiddenBlocks) {
            for (let block of this.world.hiddenBlocks) {
                this.blocks.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'block-tiles', this.world.type + '-armored').setDepth(1).setOrigin(0).refreshBody().setVisible(false); // TODO CAMBIAR ESTE BLOQUE Y HACERLO INVISIBLE !!
            }
        }

        // Flag Pole
        if (this.world.flagPole) {
            this.add.image(this.world.flagPole.c * Game.BLOCK_SIZE, this.world.flagPole.r * Game.BLOCK_SIZE, 'structure-tiles', 'flag-pole').setDepth(0).setOrigin(0);
        }        

        // Final Flag
        this.finalFlag = null;
        if (this.world.finalFlag) {
            this.finalFlag = this.blocks.create(this.world.finalFlag.c * Game.BLOCK_SIZE, this.world.finalFlag.r * Game.BLOCK_SIZE, 'final-flag').setDepth(1).setOrigin(0).refreshBody();
        }

        // Castles
        if (this.world.castles) {
            for (let block of this.world.castles) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'castle-' + block.s).setDepth(1).setOrigin(0);
            }
        }

        // Castle Flag
        if (this.world.castleFlag) {
            this.castleFlag = this.add.image(this.world.castleFlag.c * Game.BLOCK_SIZE, this.world.castleFlag.r * Game.BLOCK_SIZE, 'castle-flag').setDepth(0).setOrigin(0);
        }

        // Lifts
        if (this.world.lifts) {
            this.upLifts = this.physics.add.group({velocityY: -Game.LIFT_VELOCITY});
            this.downLifts = this.physics.add.group({velocityY: Game.LIFT_VELOCITY});
            this.horizLifts = this.physics.add.group({velocityX: Game.LIFT_VELOCITY});
            for (let block of this.world.lifts) {
                let lift = null;
                switch(block.d) {
                case 'up':
                    lift = this.upLifts.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'lift-' + block.s).setDepth(1).setOrigin(0).refreshBody();
                    lift.body.setAllowGravity(false);
                    lift.setImmovable(true);                    
                    break;
                case 'down':    
                    lift = this.downLifts.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'lift-' + block.s).setDepth(1).setOrigin(0).refreshBody();
                    lift.body.setAllowGravity(false);
                    lift.setImmovable(true);
                    break;
                case 'horiz':    
                    lift = this.horizLifts.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'lift-' + block.s).setDepth(1).setOrigin(0).refreshBody();
                    lift.body.setAllowGravity(false);
                    lift.setImmovable(true);

                    let fromX = block.c * Game.BLOCK_SIZE;
                    let toX = block.cTo * Game.BLOCK_SIZE;
                    let distance = block.cTo - block.c;
                    this.tweens.add({
                        targets: lift,
                        x: { from: fromX, to: toX },
                        ease: 'Sine.easeInOut',
                        duration: 600 * distance,
                        yoyo: true,
                        repeat: -1
                    });
                    break;
                }
            }            
        }

        // Princesses
        this.princesses = this.physics.add.staticGroup();

        if (this.world.littlePrincess) {
            let princess = this.world.littlePrincess;
            this.princesses.create(princess.c * Game.BLOCK_SIZE, princess.r * Game.BLOCK_SIZE + 8, 'structure-tiles', 'little-princess').setDepth(1).setOrigin(0).refreshBody();
        }

        if (this.world.peachPrincess) {
            let princess = this.world.peachPrincess;
            this.princesses.create(princess.c * Game.BLOCK_SIZE, princess.r * Game.BLOCK_SIZE + 8, 'structure-tiles', 'peach-princess').setDepth(1).setOrigin(0).refreshBody();
        }

        // ENEMIES
        this.enemies = this.physics.add.group({ bounceX: 1 });

        // Grumps
        if (this.world.grumps) {
            this.anims.create({
                key: 'grump-walk-anim',
                frames: this.anims.generateFrameNumbers('grump', { frames: [ 0, 1 ] }),
                frameRate: 5,
                repeat: -1
            });
        
            for (let enemy of this.world.grumps) {
                let sprite = this.enemies.create(enemy.c * Game.BLOCK_SIZE, enemy.r * Game.BLOCK_SIZE, 'grump').setDepth(1).setOrigin(0);
                sprite.play('grump-walk-anim', true);
                sprite.refreshBody();
            }
        }

        // Koopas
        if (this.world.koopas) {
            this.anims.create({
                key: 'koopa-walk-left-anim',
                frames: this.anims.generateFrameNumbers('koopa', { frames: [ 0, 1 ] }),
                frameRate: 5,
                repeat: -1
            });

            for (let enemy of this.world.koopas) {
                let sprite = this.enemies.create(enemy.c * Game.BLOCK_SIZE, enemy.r * Game.BLOCK_SIZE - 8, 'koopa').setDepth(1).setOrigin(0);
                sprite.play('koopa-walk-left-anim', true);
                sprite.refreshBody();
            }
        }

        // Bowser
        if (this.world.bowser) {
            if (!this.textures.exists('bowser')) {            
                this.textures.addSpriteSheetFromAtlas('bowser', { atlas: 'item-tiles', frame: 'bowser-sprite', frameWidth: Game.BOWSER_SIZE, frameHeight: Game.BOWSER_SIZE });
            }

            this.anims.create({
                key: 'bowser-anim',
                frames: this.anims.generateFrameNumbers('bowser', { frames: [ 0, 1, 2, 3 ] }),
                frameRate: 3,
                repeat: -1
            });

            let enemy = this.world.bowser;
            let sprite = this.enemies.create(enemy.c * Game.BLOCK_SIZE, enemy.r * Game.BLOCK_SIZE, 'bowser').setDepth(1).setOrigin(0);
            sprite.play('bowser-anim', true);
            sprite.refreshBody();

            this.bowserFire = this.physics.add.staticGroup();

            // Bowser Fire
            if (!this.textures.exists('bowser-fire')) {            
                this.textures.addSpriteSheetFromAtlas('bowser-fire', { atlas: 'item-tiles', frame: 'bowser-fire-sprite', frameWidth: 32, frameHeight: 8 });
            }

            this.anims.create({
                key: 'bowser-fire-anim',
                frames: this.anims.generateFrameNumbers('bowser-fire', { frames: [ 0, 1 ] }),
                frameRate: 2,
                repeat: -1
            });

            let fire = this.bowserFire.create(enemy.c * Game.BLOCK_SIZE - 8, enemy.r * Game.BLOCK_SIZE + 24, 'bowser-fire').setDepth(0).setOrigin(0);
            fire.play('bowser-fire-anim', true);
            fire.refreshBody();

            this.tweens.add({
                targets: [ fire, fire.body ],
                x: '-=640',
                y: '+=10',
                ease: 'Linear',
                duration: 5000,
                repeat: -1
            });                
        }

        
        // PRIZES (mush.., flor)
        this.prizes = this.physics.add.group({ bounceX: 1 });

        this.anims.create({
            key: 'mario-small-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-small', { frames: [ 1, 2, 3 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-big-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-big', { frames: [ 1, 2, 3 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-grows-anim',
            frames: this.anims.generateFrameNumbers('mario-sizes', { frames: [ 0, 1, 0, 1, 0, 1, 2, 0, 1, 2 ] }),
            frameRate: 10,
            repeat: 0
        });

        this.anims.create({
            key: 'mario-shrinks-anim',
            frames: this.anims.generateFrameNumbers('mario-sizes', { frames: [ 2, 1, 2, 1, 2, 1, 0, 2, 1, 0 ] }),
            frameRate: 10,
            repeat: 0
        });

        this.anims.create({
            key: 'mario-small-slide-down',
            frames: this.anims.generateFrameNumbers('mario-small', { frames: [ 6, 7 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-big-slide-down',
            frames: this.anims.generateFrameNumbers('mario-big', { frames: [ 6, 7 ] }),
            frameRate: 10,
            repeat: -1
        });
      
        // KEYBPOARD
        this.input.keyboard.enabled = true;
        this.cursors = this.input.keyboard.createCursorKeys();

        // COLLISIONS
        // Bloques
        this.physics.add.collider(this.mario, [this.blocks, this.upLifts, this.downLifts, this.horizLifts]);

        for (let fireball of this.fireballs) {
            this.physics.add.collider(this.mario, fireball, (mario, ball) => {
                if (mario.visible) {
                    this.mario.die();
                }
            });
        }   

        this.physics.add.collider(this.mario, this.prizes, (mario, prize) => {
            if (mario.visible) {
                prize.destroy();
                this.mario.grow();
            }
        });

        // Overlap with enemies
        this.physics.add.overlap(this.mario, this.enemies, (mario, enemy) => {
            if (!this.mario.isProtected() && this.mario.isAlive() && !enemy.die) {
                if (!enemy.body.touching.up) {
                    this.mario.hurt();
                } else {
                    this.mario.hurtEnemy(enemy);
                }
            }
        });

        this.physics.add.collider(this.mario, this.bowserFire, (mario, fire) => {
            if (mario.visible) {
                this.mario.die();
            }
        });

        // Overlap with coins
        this.physics.add.overlap(this.mario, this.coins, (mario, coin) => {
            if (this.audio) this.coinSound.play();
            this.incCoins();
            coin.destroy();
        });

        this.physics.add.collider(this.mario, this.princesses, (mario, princess) => {
            this.restartGame();
        });

        this.physics.add.collider(this.prizes, this.blocks);

        this.physics.add.collider(this.enemies, this.blocks);

        // Question Blocks
        this.physics.add.collider(this.mario, this.questionBlocks, (mario, block) => {
            if (!block.immovable && block.body.touching.down) {
                switch(block.t) {
                case 'coin':
                    let coin = this.add.sprite(block.x, block.y - 16, block.t).setDepth(1).setOrigin(0);
                    coin.play('spincoin-anim', true);
                    if (this.audio) this.coinSound.play();
                    this.incCoins();
                    this.addFlyingPoints(block.x, block.y - 16, block.p);

                    this.tweens.add({
                        targets: coin,
                        y: { from: coin.y, to: coin.y - 48 },
                        ease: 'Cubic',
                        duration: 500,
                        yoyo: true,
                        onComplete: () => {
                            coin.destroy();
                        },
                    });                
                    break;    
                case 'red-m':
                case 'green-m':
                    let mushroom = this.prizes.create(block.x - 8, block.y, block.t).setDepth(0).setOrigin(0).refreshBody();
                    mushroom.setImmovable(true); // Whether this Body can be moved by collisions with another Body
                    mushroom.body.allowGravity = false;

                    if (this.audio) this.mushroomSound.play();
                    this.addFlyingPoints(block.x, block.y - 16, block.p);

                    this.tweens.add({
                        targets: mushroom,
                        y: { from: mushroom.y - 8, to: mushroom.y - 16 },
                        ease: 'Linear',
                        duration: 1000,
                        onComplete: () => {
                            mushroom.setVelocityX(60);
                            mushroom.setImmovable(false);
                            mushroom.body.allowGravity = true;
                        },
                    });                
    
                    break;    
                }

                block.immovable = true;
                block.anims.stop();
                block.setFrame(3);
                this.tweens.add({
                    targets: block,
                    y: { from: block.y, to: block.y - 8 },
                    ease: 'Cubic',
                    duration: 100,
                    repeat: 0,
                    yoyo: true
                });
            }
        });

        // AUDIOS
        this.jumpSmall = this.sound.add('jump-small', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: false,
            delay: 0
        });

        this.jumpBig = this.sound.add('jump-big', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.powerUp = this.sound.add('power-up', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.coinSound = this.sound.add('coin-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.mushroomSound = this.sound.add('mushroom-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.stompSound = this.sound.add('stomp-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.dieSound = this.sound.add('die-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.pointsSound = this.sound.add('points-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });        
        this.flagPoleSound = this.sound.add('flag-pole', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.worldClear = this.sound.add('stage-clear', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.pipeSound = this.sound.add('pipe-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.music = this.sound.add(this.world.music, { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: true, delay: 0 });
        if (this.audio) this.music.play();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DEBUG MODE - ESC KEY
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.physics.world.drawDebug = false;
        this.input.keyboard.addKey('ESC').on('down', (event) => {
            this.debug = !this.debug;
            this.header.toggleTimer(this.debug);
        });

        this.input.keyboard.addKey('D').on('down', (event) => {
            this.physics.world.debugGraphic.clear();
            this.physics.world.drawDebug = !this.physics.world.drawDebug;
        });

        this.input.keyboard.addKey('HOME').on('down', (event) => {
            if (this.debug) {
                this.cameras.main.scrollX = 0;
            }
        });

        this.input.keyboard.addKey('END').on('down', (event) => {
            if (this.debug) {
                this.cameras.main.scrollX = this.world.width - Game.SCENE_WIDTH;
            }
        });

        this.input.keyboard.addKey('PAGE_UP').on('down', (event) => {
            if (this.debug) {
                if (this.cameras.main.scrollX > 0) { // Left
                    let step = this.cameras.main.scrollX - Game.SCENE_WIDTH < 0 ? this.cameras.main.scrollX : Game.SCENE_WIDTH;
                    this.cameras.main.scrollX -= step; // Move the camera
                }
            }
        });

        this.input.keyboard.addKey('PAGE_DOWN').on('down', (event) => {
            if (this.debug) {
                if (this.cameras.main.scrollX < this.world.width - Game.SCENE_WIDTH) { // Right
                    let step = this.cameras.main.scrollX + Game.SCENE_WIDTH > this.world.width - Game.SCENE_WIDTH ? this.world.width - Game.SCENE_WIDTH - this.cameras.main.scrollX : Game.SCENE_WIDTH;
                    this.cameras.main.scrollX += step; // Move the camera
                }
            }
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    update() {
        let camera = this.cameras.main;

        // Lifts
        if (this.world.lifts) {
            this.upLifts.getChildren().forEach((lift) => {
                if (lift.y < 0) {
                    lift.setY(this.world.height);
                }
            }, this);
            this.downLifts.getChildren().forEach((lift) => {
                if (lift.y > this.world.height) {
                    lift.setY(0);
                }
            }, this);
        }

        // Rotation Fireball Sticks
        for (let fireball of this.fireballs) {
            Phaser.Actions.RotateAroundDistance(fireball.getChildren(), { x: fireball.x, y: fireball.y }, fireball.angle, fireball.distance);       
            
            // Updtae fireballs body
            for (const ball of fireball.getChildren()) {
                ball.body.x = ball.x;
                ball.body.y = ball.y;
                ball.body.angle = ball.angle;
            }
        }   
                
        // DEBUG MODE
        if (this.debug) {
            if (this.cursors.left.isDown && camera.scrollX > 0) { // Left
                camera.scrollX -= Game.CAMERA_VELOCITY * 3; // Move the camera
            } else if (this.cursors.right.isDown && camera.scrollX < this.world.width - Game.SCENE_WIDTH) { // Right
                camera.scrollX += Game.CAMERA_VELOCITY * 3; // Move the camera
            }            
            return;
        }

        if (this.worldEnds) {            
            return;
        }

        if (this.mario.y > this.world.height - this.mario.height) { // Check if Mario falls into a pit
            this.mario.die();
        } else if (this.finalFlag && this.mario.x > this.finalFlag.x) { // Check id Mario arrive final flag
            this.input.keyboard.enabled = false;
            // Posiciona a Mario por encima de la bandera
            if (!this.mario.isSliding()) {
                if (this.mario.y < this.finalFlag.y) {
                    this.mario.y = this.finalFlag.y - this.mario.height;
                }
                this.mario.slideDown(this.finalFlag.x + 8);
                if (this.audio) this.flagPoleSound.play();
                // Remove timer
                this.header.stopTimer();
            }

            this.mario.disableBody(false, false);
            
            if ((this.mario.y > this.finalFlag.y && this.mario.y < 176) || (this.mario.y <= this.finalFlag.y && this.finalFlag.y < 168)) {
                this.mario.y += 2;
                this.finalFlag.y += 2;
            } else {
                this.music.stop();
                if (this.audio) this.worldClear.play();
                this.mario.goCastleDoor();
                this.worldEnds = true;
            }        

        } else if (this.mario.isAlive()) { // Keyboard controls
            if (this.cursors.left.isDown) { // Left
                this.mario.turnLeft();
                if (this.mario.x > camera.scrollX) {
                    this.mario.run();
                } else {
                    this.mario.stop(camera.scrollX);
                }
            } else if (this.cursors.right.isDown) { // Right
                this.mario.turnRight();
                if (this.mario.x < camera.scrollX + Game.CAMERA_MOTION_POINT || camera.scrollX >= this.world.width - Game.SCENE_WIDTH) {
                    if (this.mario.x < this.world.width - this.mario.width) {
                        this.mario.run();
                    } else {
                        this.mario.stop(this.world.width - this.mario.width);
                    }
                } else {
                    if (camera.scrollX < this.world.width - Game.SCENE_WIDTH) {
                        camera.scrollX += Game.CAMERA_VELOCITY; // Move the camera
                    }
                }
            } else { 
                this.mario.stand();
            }

            if ((this.cursors.space.isDown || this.cursors.up.isDown) && this.mario.isOnGround()) {
                this.mario.jump();
            }
            
            // Enemies movements
            this.moveEnemies();
        } else { // !isAlive
            this.mario.dying();
        }

    }

    moveEnemies() {
        for (const enemy of this.enemies.getChildren()) {
            // Check if enemy is in camera viewport
            if (this.cameras.main.worldView.contains(enemy.x, enemy.y)) {
                if (enemy.body.velocity.x == 0) {
                    enemy.setVelocityX(Game.ENEMY_VELOCITY);
                }
            }
        }
    }

    stopEnemies() {
        this.enemies.setVelocityX(0);
        for (const enemy of this.enemies.getChildren()) {
            enemy.anims.stop();
        }
    }

    addPoints(points) {
        this.header.setScore(points);
    }

    incCoins() {
        this.addPoints(200);
        this.header.incCoins();
    }

    addFlyingPoints(x, y, points) {
        let flyPoints = this.add.image(x, y - 16, points).setDepth(1).setOrigin(0);
        this.tweens.add({
            targets: flyPoints,
            x: '+=32',
            y: '-=32',
            ease: 'Linear',
            duration: 2500,
            onComplete: () => {
                flyPoints.destroy();     
            },
        });
    }
    
    endWorld() {
        this.time.addEvent({
            delay: 20,
            repeat: this.header.getTime()-1,
            callback: () => {
                this.header.decTime();
                if (this.audio) this.pointsSound.play();
                this.addPoints(100);
                if (this.header.getTime() == 0) {
                    this.pointsSound.stop();
                    // Castle flag appear
                    this.tweens.add({
                        targets: this.castleFlag,
                        y: '-=32',
                        ease: 'Linear',
                        duration: 800,
                        yoyo: false,
                        onComplete: () => {
                            // Next World
                            if (this.world.nextStageId) {
                                let data = {
                                    stageId: this.world.nextStageId,
                                    skip: false,
                                    marioSize: this.mario.getSize(),
                                    score: this.header.getScore(),
                                    coins: this.header.getCoins()
                                };
                                this.scene.start('World', data );
                            } else {
                                this.restartGame();                                
                            }
                        }
                    });
                }
            }
        });

    } // endWorld

    restartGame() {
        // Restart the scene after 3 seconds with the last score
        this.time.addEvent({
            delay: Game.END_GAME_DELAY,
            callback: () => {
                this.scene.start('Main', { score: this.header.getScore() });
            }
        });
    } // restartGame

}