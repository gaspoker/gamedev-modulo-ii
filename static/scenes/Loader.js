/**
 * Loader class
 */
export class Loader extends Phaser.Scene {
    constructor () {
        super('Loader'); // Scene key
    }

    preload() {
        // Fonts
        this.load.bitmapFont('font-white', 'assets/fonts/font-white.png', 'assets/fonts/font.fnt');
        this.load.bitmapFont('font-pink', 'assets/fonts/font-pink.png', 'assets/fonts/font.fnt');

        // Block Tiles 
        this.load.atlas('block-tiles', 'assets/images/blocks/block-tiles.png', 'assets/images/blocks/block-tiles.json');

        // Structure Tiles 
        this.load.atlas('structure-tiles', 'assets/images/blocks/structure-tiles.png', 'assets/images/blocks/structure-tiles.json');

        // Item Tiles 
        this.load.atlas('item-tiles', 'assets/sprites/item-tiles.png', 'assets/sprites/item-tiles.json');

        this.load.image('player-mushroom', 'assets/images/player-mushroom.png');
        this.load.image('banner', 'assets/images/banner.png');
        this.load.image('score-coin', 'assets/images/score-coin-shadow.png');
        
        // MARIO
        this.load.spritesheet('mario-small', 'assets/sprites/mario-small.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('mario-big', 'assets/sprites/mario-big.png', { frameWidth: 16, frameHeight: 32 });
        this.load.spritesheet('mario-sizes', 'assets/sprites/mario-sizes.png', { frameWidth: 16, frameHeight: 32 });

        // ENEMIES
        this.load.spritesheet('grump', 'assets/sprites/grump.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('koopa', 'assets/sprites/koopa.png', { frameWidth: 16, frameHeight: 24 });

        // POINTS
        this.load.image('100', 'assets/images/points/100.png');
        this.load.image('200', 'assets/images/points/200.png');
        this.load.image('400', 'assets/images/points/400.png');
        this.load.image('500', 'assets/images/points/500.png');
        this.load.image('800', 'assets/images/points/800.png');
        this.load.image('1000', 'assets/images/points/1000.png');
        this.load.image('2000', 'assets/images/points/2000.png');
        this.load.image('4000', 'assets/images/points/4000.png');
        this.load.image('5000', 'assets/images/points/5000.png');
        this.load.image('8000', 'assets/images/points/8000.png');
        this.load.image('1UP', 'assets/images/points/1UP.png');

        // BLOCKS
        this.load.image('red-m', 'assets/images/blocks/red-mushroom.png');
        this.load.image('green-m', 'assets/images/blocks/green-mushroom.png');

        this.load.image('final-flag', 'assets/images/blocks/final-flag.png');
        this.load.image('castle-flag', 'assets/images/blocks/castle-flag.png');

        // AUDIO
        this.load.audio('jump-small', 'assets/audios/jump-small.mp3');
        this.load.audio('jump-big', 'assets/audios/jump-big.mp3');
        this.load.audio('power-up', 'assets/audios/power-up.mp3');
        this.load.audio('coin-sound', 'assets/audios/coin.mp3');
        this.load.audio('mushroom-sound', 'assets/audios/mushroom.mp3');
        this.load.audio('stomp-sound', 'assets/audios/stomp.mp3');
        this.load.audio('die-sound', 'assets/audios/die.mp3');
        this.load.audio('points-sound', 'assets/audios/points.mp3');
        this.load.audio('stage-clear', 'assets/audios/stage-clear.mp3');    
        this.load.audio('flag-pole', 'assets/audios/flag-pole.mp3');
        this.load.audio('pipe-sound', 'assets/audios/pipe.mp3');
        this.load.audio('overworld', 'assets/audios/overworld.mp3');        
        this.load.audio('underground', 'assets/audios/underground.mp3');        
    }

    create() {
        this.scene.start('Main');
    }
}

