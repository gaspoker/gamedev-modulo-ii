import { Game } from './Game.js';
import { Mario } from '../models/Mario.js';
import { Header } from '../models/Header.js';

/**
 * Class Intro
 */
export class Intro extends Phaser.Scene {

    constructor() {
        super('Intro');
    }

    create(data) {      
        let intro = data.world.intro;

        // HEADER
        new Header(this, data.score, data.coins, data.stage, data.time);

        // MARIO
        this.mario = new Mario(this, intro.marioX, intro.marioY, data.marioSize);

        this.cameras.main.setBackgroundColor(intro.color);

        // Clouds
        if (intro.clouds) {
            for (let block of intro.clouds) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'cloud-' + block.s).setDepth(0).setOrigin(0);
            }
        }       

        // Mountains
        if (intro.mountains) {
            for (let block of intro.mountains) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'mountain-' + block.s).setDepth(0).setOrigin(0);
            }
        }       

        // Bushes
        if (intro.bushes) {
            for (let block of intro.bushes) {
                this.add.image(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', 'bush-' + block.s).setDepth(0).setOrigin(0);
            }
        }
        
        // Castle
        if (intro.castle) {
            this.add.image(intro.castle.c * Game.BLOCK_SIZE, intro.castle.r * Game.BLOCK_SIZE, 'structure-tiles', 'castle-' + intro.castle.s).setDepth(1).setOrigin(0);
        }

        // Ground Blocks
        this.blocks = this.physics.add.staticGroup();
        if (intro.groundBlocks) {
            for (let block of intro.groundBlocks) {
                this.blocks.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'block-tiles', intro.type + '-ground').setDepth(1).setOrigin(0).refreshBody();
            }
        }

        // Pipe Tubes
        if (intro.pipeTubes) {
            for (let block of intro.pipeTubes) {
                this.blocks.create(block.c * Game.BLOCK_SIZE, block.r * Game.BLOCK_SIZE, 'structure-tiles', intro.type + '-' + block.t).setDepth(2).setOrigin(0).refreshBody();
            }
        }

        this.physics.add.collider(this.mario, this.blocks);

        // Mario Run
        this.tweens.add({
            targets: this.mario,
            //x: '+=112',
            x: { from: intro.marioX, to: intro.marioToX },
            ease: 'Linear',
            duration: intro.duration - 500,
            yoyo: false,
            onStart: () => {                        
                this.mario.running();
            },
            onComplete: () => {
                this.pipeSound.play();
                this.tweens.add({
                    targets: this.mario,
                    x: '+=16',
                    ease: 'Linear',
                    duration: 500,
                    onComplete: () => {
                        this.time.addEvent({
                            delay: 2000,
                            callback: () => {
                                this.startGame(data);
                            },
                            loop: false
                        });                                
                    },
                });
            }
        });

        this.anims.create({
            key: 'mario-small-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-small', { frames: [ 1, 2, 3 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'mario-big-runs-anim',
            frames: this.anims.generateFrameNumbers('mario-big', { frames: [ 1, 2, 3 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.pipeSound = this.sound.add('pipe-sound', { mute: false, volume: 1, rate: 1, detune: 0, seek: 0, loop: false, delay: 0 });
        this.physics.world.drawDebug = false;
    }

    startGame(data) {
        this.scene.start('Game', data);
    }

}